#include <stdio.h>
#include <stdlib.h>
#include<math.h>
#include <pthread.h>

int NTHREADS;
int NQUEENS;
// int TDIM;

/* @mrj */

pthread_t** threads;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

struct tblr{
	int** tab;
	int dim;
	// int nThread;
};
typedef struct tblr Tabuleiro;

struct thrd{
	Tabuleiro* nTab;
	int inseridas;
	int linha;
	unsigned long long* soma;
	int nThread;
};
typedef struct thrd parametrosThread;



Tabuleiro* inicializaTabuleiro(int dim);
void printaTabuleiro(Tabuleiro* a);
void insereRainhas(Tabuleiro* a, int inseridas, unsigned long long* soma, int linha);
parametrosThread* reuneParametros(Tabuleiro* a, int inseridas, unsigned long long* soma, int linha);
void* solve(void* p);
void checaMemoria (void* p);

int nqueens(int dim, int queens){

	Tabuleiro *a;
	unsigned long long soma = 0;

	if(queens > dim || dim < 1 || queens < 1){
		return 0;
	}

	if(dim == 1){
		return 1;
	}


	NTHREADS = 2;
	NQUEENS = queens;
	// TDIM = dim;

	a = inicializaTabuleiro(dim);

	insereRainhas(a, 0, &soma, 0);

	for(int i = 0; i < NTHREADS; i++){
		pthread_join(*threads[i], NULL);
		free(threads[i]);
	}

	return soma;
}

Tabuleiro* inicializaTabuleiro(int dim){

	Tabuleiro *a = malloc(sizeof(Tabuleiro));
	checaMemoria(a);

	a->dim = dim;

	a->tab = (int**)malloc(dim * sizeof(int*));
	checaMemoria(a->tab);

	for (int i = 0; i < dim; i++){ 
       a->tab[i] = (int*) malloc(dim * sizeof(int));
       checaMemoria(a->tab[i]); 
       for (int j = 0; j < dim; j++){ 
       		a->tab[i][j] = 0; 
       }
  	}

	return a; 
}

void printaTabuleiro(Tabuleiro* a){

	for(int i = 0; i < a->dim; i++){
		for(int j = 0; j < a->dim; j++){
			printf("%d", a->tab[i][j]);
		}
		printf("\n");
	}
}

int insercaoValida(Tabuleiro* a, int linha, int col){

    int i, j;

    for (i = 0; i < linha; i++)
        if (a->tab[i][col] == 1)
            return 0;

    i = linha; j = col;
    while (i >= 0 && j >= 0){
    	if (a->tab[i][j] == 1)
            return 0;
     	i--; j--;
    }

    i = linha; j = col;
    while (i >= 0 && j < a->dim){
    	if (a->tab[i][j] == 1)
            return 0;
     	i--; j++;
    }

    return 1;
}


unsigned long long calcSoma(Tabuleiro* a){

	int pos = 0;
	unsigned long long soma = 0;

	for(int i = 0; i < a->dim; i++){
		for(int j = 0; j < a->dim; j++){
			if(a->tab[i][j] == 1){
				soma += pow(2, pos);
			}
			pos++;
		}
	}
	// printf("custo: %llu\n",soma);
	// printf("--------\n");
	return soma;
}

void insereRainhas(Tabuleiro* a, int inseridas, unsigned long long* soma, int linha){

    parametrosThread *p = NULL;

    if (inseridas == NQUEENS){
    	pthread_mutex_lock(&mutex);
    	// printaTabuleiro(a);
    	// printf("--------\n");
    	*soma += calcSoma(a);
    	// printf("soma: %d\n", soma);
    	pthread_mutex_unlock(&mutex);

    	free(a);

    	return;
    }

    for (int i = 0; i < a->dim; i++){
    	for(int j = linha; j < a->dim; j++){
	        if (insercaoValida(a, j, i)){
	            a->tab[j][i] = 1;

	            p = reuneParametros(a, inseridas, soma, j);

	            if (pthread_create(*(&threads[p->nThread]), NULL, solve, (void*) p) != 0){
	            	printf("erro ao criar thread[%d]\n", NTHREADS);
		        }

	            a->tab[j][i] = 0;
	        }
	    }
    }
}


parametrosThread* reuneParametros(Tabuleiro* a, int inseridas, unsigned long long* soma, int linha){

	parametrosThread *p = malloc(sizeof(parametrosThread));
	checaMemoria(p);
	
	p->nTab = malloc(sizeof(Tabuleiro));
	checaMemoria(p->nTab);

	p->nTab->dim = a->dim;	
	p->nTab->tab = (int**)malloc(a->dim * sizeof(int*));
	checaMemoria(p->nTab->tab);

	for (int i = 0; i < p->nTab->dim; i++){ 
       p->nTab->tab[i] = (int*) malloc(p->nTab->dim * sizeof(int));
       checaMemoria(p->nTab->tab[i]); 
       for (int j = 0; j < p->nTab->dim; j++){ 
       		p->nTab->tab[i][j] = a->tab[i][j]; 
       }
  	}

	p->inseridas = inseridas;
	p->soma = soma;
	p->linha = linha;

	pthread_mutex_lock(&mutex);
	p->nThread = NTHREADS;
	threads = realloc(threads, sizeof(pthread_t*)*NTHREADS+1);
	checaMemoria(threads);
    threads[NTHREADS] = malloc(sizeof(pthread_t));
    checaMemoria(threads[NTHREADS]);
	NTHREADS++;
	pthread_mutex_unlock(&mutex);
	
	return p;
}


void* solve(void* p){

	parametrosThread *parametros;
	parametros = (parametrosThread *) p;

    insereRainhas(parametros->nTab, parametros->inseridas+1, parametros->soma, parametros->linha+1);

    free(parametros);

    return NULL;
}


void checaMemoria (void* p){

	if (p != NULL){
		return;
	}

	printf("erro ao alocar mem�ria\n");
	exit(1);
	// return 0;
}